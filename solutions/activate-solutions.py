#Author http://stph.crzt.fr
#Version 2017-02-16

"""
Scenari Model : 
    Opale 3.6
Purpose :
    Activate all the filters for all pedagogical blocs situated in any solution of any exercice 
Documentation :
    1. Export : a scar from your ScenariChain
    2. Execute : python activate-solutions.py myfile.scar
    3. Retrieve : the result from 'activate-solutions' in your system temporary directory and drag and drop it to a Scenari Chain dedicated space 
"""

import os
import shutil
import tempfile
import sys
import zipfile
import xml.etree.ElementTree as ET

#Initialisation
scarFile = sys.argv[1]
tmpDir = tempfile.gettempdir()
deliveryDir = tmpDir + "/activate-solutions"
ns = {"sp":"http://www.utc.fr/ics/scenari/v3/primitive", "op":"utc.fr:ics/opale3"}
print "Doing " + scarFile
print "Delivery to " + deliveryDir

#Creating delivery dir
print "Initialising " + deliveryDir + " for delivery"
shutil.rmtree(deliveryDir, ignore_errors=1)
os.mkdir(deliveryDir)

#Unzipping in tmpDir
scar = zipfile.ZipFile(scarFile) 
scar.extractall(deliveryDir)

#Treating
found = 0
for path, dirs, files in os.walk(deliveryDir):	
	for f in files:
		if ".xml" in f and f not in ["meta.xml","props.xml"]:
			xml = path + "/" + f			
			tree = ET.parse(open(path + "/" + f,"r"))	
			pbTiWithFilters = tree.findall(".//sp:sol//op:pbTi[sp:filters]",ns)
			if (pbTiWithFilters):
				print path + "/" + f + " activated"	
				found=1
				for ti in pbTiWithFilters:
					filters = ti.find("sp:filters",ns)			
					ti.remove(filters)				
				tree.write(xml)
if found==0:
	print "nothing to do"
			

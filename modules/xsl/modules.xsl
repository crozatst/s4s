<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:of="scpf.org:office"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
            <of:fragment xmlns:of="scpf.org:office"
                xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
                <xsl:apply-templates select="modules/section"/>
            </of:fragment>
        </sc:item>
    </xsl:template>

    <xsl:template match="section" mode="#all">
        <xsl:param name="na"/>
        <sp:info>
            <of:block>
                <of:blockM>
                    <xsl:if test="@title">
                        <sp:title>
                            <xsl:value-of select="@title"/>
                        </sp:title>
                    </xsl:if>
                </of:blockM>
                <sp:co>
                    <of:flow>
                        <sp:txt>
                            <of:txt>
                                <xsl:choose>
                                    <xsl:when test="sequence">
                                        <xsl:apply-templates select="sequence"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:call-template name="insertTable"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </of:txt>
                        </sp:txt>
                    </of:flow>
                </sp:co>
            </of:block>
        </sp:info>
    </xsl:template>

    <xsl:template match="sequence">
        <xsl:call-template name="insertTable">
            <xsl:with-param name="caption">
                <xsl:choose>
                    <xsl:when test="@title">
                        <xsl:value-of select="@title"/>
                    </xsl:when>
                    <xsl:otherwise>
                        Séquence <xsl:value-of select="count(preceding::sequence)+1"/>
                    </xsl:otherwise>
                </xsl:choose>                
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="insertTable">
        <xsl:param name="caption"/>
        <sc:table role="">
            <sc:caption>
                <xsl:value-of select="$caption"/>
            </sc:caption>
            <sc:column role="total" width="5"/>
            <sc:column width="100"/>
            <xsl:if test="//web">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:if test="//pdf">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:if test="//prs">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:if test="//vid">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:if test="//wm">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:if test="//lin">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:if test="//time">
                <sc:column width="10"/>
            </xsl:if>
            <xsl:apply-templates select="module"/>
        </sc:table>
    </xsl:template>

    <xsl:template name="setUrl">
        <xsl:param name="code"/>
        <xsl:param name="title"/>
        <xsl:param name="icon"/>
        <sc:para>
            <sc:phrase role="url">
                <of:urlM>
                    <sp:url>
                        <xsl:value-of select="$code"/>
                    </sp:url>
                </of:urlM>                
                <xsl:if test="$icon">
                    <sc:inlineImg role="ico" sc:refUri="{$icon}"/>
                </xsl:if>
                <xsl:if test="$title">
                    <xsl:value-of select="$title"/>
                </xsl:if>            
            </sc:phrase>
        </sc:para>
    </xsl:template>

    <xsl:template match="module">

        <xsl:variable name="url">
            <xsl:choose>
                <xsl:when test="url">
                    <xsl:value-of select="url"/>
                </xsl:when>
                <xsl:when test="ancestor::section/url">
                    <xsl:value-of select="ancestor::section/url"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="/modules/url"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- 
        <xsl:variable name="web">id:ESFyiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="webc">id:ESNyiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="pdf">id:ESByiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="prs">id:ES7yiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="vid">id:ESJyiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="wm">id:ES3yiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="lin">id:ESRyiZrOK77A7dgFJsXoCm</xsl:variable>
        <xsl:variable name="none">id:PFLyiZrOK77A7dgFJsXoCm</xsl:variable>
        -->
        <xsl:variable name="icons">icons</xsl:variable>
        <xsl:variable name="web"><xsl:value-of select="$icons"/>/<xsl:value-of>web.png</xsl:value-of></xsl:variable>       
        <xsl:variable name="pdf"><xsl:value-of select="$icons"/>/<xsl:value-of>pdf.png</xsl:value-of></xsl:variable>
        <xsl:variable name="prs"><xsl:value-of select="$icons"/>/<xsl:value-of>prs.png</xsl:value-of></xsl:variable>
        <xsl:variable name="vid"><xsl:value-of select="$icons"/>/<xsl:value-of>vid.png</xsl:value-of></xsl:variable>
        <xsl:variable name="wm"><xsl:value-of select="$icons"/>/<xsl:value-of>wm.png</xsl:value-of></xsl:variable>
        <xsl:variable name="lin"><xsl:value-of select="$icons"/>/<xsl:value-of>lin.png</xsl:value-of></xsl:variable>
        <xsl:variable name="none"><xsl:value-of select="$icons"/>/<xsl:value-of>web-none.png</xsl:value-of></xsl:variable>
        
        <xsl:variable name="defaultSupports" select="not(web | pdf | prs | vid | wm | lin | none)"/>

        <sc:row>
            <sc:cell role="num">
                <sc:para>
                    <xsl:value-of select="count(preceding::module)+1"/>
                </sc:para>
            </sc:cell>
            <sc:cell>
                <xsl:choose>
                    <xsl:when test="web or ($defaultSupports and /modules/web)">
                        <xsl:call-template name="setUrl">
                            <xsl:with-param name="code">
                                <xsl:value-of select="$url"/>/<xsl:value-of select="code"/>
                            </xsl:with-param>
                            <xsl:with-param name="title">
                                <xsl:value-of select="title"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <sc:para>
                            <xsl:value-of select="title"/>
                        </sc:para>
                    </xsl:otherwise>
                </xsl:choose>

            </sc:cell>
            <xsl:if test="//web">
                <sc:cell role="word">
                    <xsl:choose>
                        <xsl:when test="web or $defaultSupports and /modules/web">
                            <xsl:call-template name="setUrl">
                                <xsl:with-param name="code">
                                    <xsl:value-of select="$url"/>/<xsl:value-of select="code"/>
                                </xsl:with-param>
                                <xsl:with-param name="icon">
                                    <xsl:value-of select="$web"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="none">
                            <sc:para><sc:inlineImg role="ico" sc:refUri="{$none}"/></sc:para>
                        </xsl:when>
                    </xsl:choose>                    
                </sc:cell>
            </xsl:if>
            <xsl:if test="//pdf">
                <sc:cell role="word">
                    <xsl:if test="pdf or $defaultSupports and /modules/pdf">
                        <xsl:call-template name="setUrl">
                            <xsl:with-param name="code"><xsl:value-of select="$url"/>/<xsl:value-of
                                    select="code"/>.pdf</xsl:with-param>
                            <xsl:with-param name="icon">
                                <xsl:value-of select="$pdf"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </sc:cell>
            </xsl:if>
            <xsl:if test="//prs">
                <sc:cell role="word">
                    <xsl:if test="prs or $defaultSupports and /modules/prs">
                        <xsl:call-template name="setUrl">
                            <xsl:with-param name="code"><xsl:value-of select="$url"/>/<xsl:value-of
                                    select="code"/>-prs</xsl:with-param>
                            <xsl:with-param name="icon">
                                <xsl:value-of select="$prs"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </sc:cell>
            </xsl:if>
            <xsl:if test="//vid">
                <sc:cell role="word">
                    <xsl:if test="vid or $defaultSupports and /modules/vid">
                        <xsl:call-template name="setUrl">
                            <xsl:with-param name="code">http://bdd.crzt.fr/<xsl:value-of
                                    select="code"/>/vid</xsl:with-param>
                            <xsl:with-param name="icon">
                                <xsl:value-of select="$vid"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </sc:cell>
            </xsl:if>
            <xsl:if test="//wm">
                <sc:cell role="word">
                    <xsl:if test="wm or $defaultSupports and /modules/wm">
                        <xsl:call-template name="setUrl">
                            <xsl:with-param name="code"><xsl:value-of select="$url"/>/<xsl:value-of
                                    select="code"/>-wm</xsl:with-param>
                            <xsl:with-param name="icon">
                                <xsl:value-of select="$wm"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </sc:cell>
            </xsl:if>
            <xsl:if test="//lin">
                <sc:cell role="word">
                    <xsl:if test="lin or $defaultSupports and /modules/lin">
                        <xsl:call-template name="setUrl">
                            <xsl:with-param name="code"><xsl:value-of select="$url"/>/<xsl:value-of
                                    select="code"/>-lin</xsl:with-param>
                            <xsl:with-param name="icon">
                                <xsl:value-of select="$lin"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                </sc:cell>
            </xsl:if>
            <xsl:if test="//time">
                <sc:cell role="word">
                    <xsl:choose>
                        <xsl:when test="time">
                            <sc:para>[<xsl:value-of select="time"/><xsl:value-of select="time/@unit"
                                />]</sc:para>
                        </xsl:when>
                        <xsl:otherwise>
                            <sc:para>[<xsl:value-of select="/modules/time"/><xsl:value-of
                                    select="/modules/time/@unit"/>]</sc:para>
                        </xsl:otherwise>
                    </xsl:choose>
                </sc:cell>
            </xsl:if>
        </sc:row>

    </xsl:template>



</xsl:stylesheet>

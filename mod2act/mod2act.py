#!/usr/bin/python3
#Author http://stph.crzt.fr
#Version 2018-02-07

"""
Scenari Model :
    Opale 3.6
Purpose :
    Transform a module (ue) to courseUa
Techical documentation
    1)  from : <op:ue xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
        to : <op:courseUa xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    2)  from : <op:ueM>
        to : <op:uM>
Documentation :
    1. Search for -devoir.xml items in your your ScenariChain
    2. Export : a scar
    3. Execute : python mod2act.py myfile.scar
    3. Retrieve : the result from 'mod2act' in your system temporary directory and drag and drop it to a Scenari Chain dedicated space
TODO
    - Optimize :
        - os.walk inutile (we only need first level files)
        - imbricated replace ; choose a map function
"""

import os
import shutil
import tempfile
import sys
import zipfile
import xml.etree.ElementTree as ET

#Initialisation
scarFile = sys.argv[1]
tmpDir = tempfile.gettempdir()
deliveryDir = tmpDir + "/mod2act"
dirout = "__out"
ns = {"sp":"http://www.utc.fr/ics/scenari/v3/primitive", "op":"utc.fr:ics/opale3"}
print "Doing " + scarFile
print "Delivery to " + deliveryDir

#Creating delivery dir
print "Initialising " + deliveryDir + " for delivery"
shutil.rmtree(deliveryDir, ignore_errors=1)
os.mkdir(deliveryDir)
os.mkdir("%s/%s" % (deliveryDir, dirout))

#Unzipping in tmpDir
scar = zipfile.ZipFile(scarFile)
scar.extractall(deliveryDir)

#Treating
found = 0
for path, dirs, files in os.walk(deliveryDir):
    for f in files:
        if "-devoir.xml" in f:
            fin_name = "%s/%s" % (deliveryDir, f)
            fout_name = "%s/%s/%s" % (deliveryDir, dirout, f.replace("-devoir.xml","-devoir_a.xml"))
            print("%s > %s" % (fin_name, fout_name))
            fin = open (fin_name, 'r')
            fout = open (fout_name,'w')
            for line in fin:
                fout.write(line.replace("<op:ue xmlns:", "<op:courseUa xmlns:").replace("<op:ueM>", "<op:uM>").replace("</op:ue>","</op:courseUa>").replace("</op:ueM>","</op:uM>").replace('sc:refUri="&amp;/','sc:refUri="/'))
        else:
            print("%s ignored (not a -devoir.xml file)" % f)

if found==0:
	print "nothing to do"
